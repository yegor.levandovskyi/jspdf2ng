import { Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import jsPDF from "jspdf";
import html2canvas from 'html2canvas';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  form: FormGroup;
  images: string[] = [];
  doc: jsPDF;

  constructor(private fb: FormBuilder) {}
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.form = this.fb.group({
      image: this.fb.control(null)
    });

    this.form.get("image").valueChanges.subscribe(value => {
      this.images = value;
    });
  }

  saveToPdf() {
    const height = 100;
    const width = 100;
    this.doc = new jsPDF("p", "px", "a4");
    this.images.forEach((image, index) => {
      this.doc.addImage(image, "JPEG", 0 + index * width, 0, width, height);
    });
    this.doc.save('sudas.pdf');
  }
}
