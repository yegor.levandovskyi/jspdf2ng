import { Directive, forwardRef, ElementRef, Renderer2, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Directive({
  selector: "input[type=file]",
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileInputValueAccessorDirective),
      multi: true,
    }
  ]
})

export class FileInputValueAccessorDirective implements ControlValueAccessor {
  images: string[] = [];

  constructor() { }

  onChange: any = () => { };

  @HostListener('change', ['$event.target.files']) _handleInput(event) {
    this.changeListener(event);
    this.onChange(this.images);
  }

  changeListener(files) {
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.images.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }

  writeValue(value: any): void {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }
}
